﻿Farmas.getCurrentDate = function() {
    return "TESTDATE";
}

Farmas.getHostName = function() {
    return "TESTHOST";
}

describe("Dashboard", function() {
    beforeEach(function() {
        jasmine.getFixtures().set("<div id='dashboardContainer' />");
    });

    describe("constructor", function() {
        it("should build ajax request to get projects from CCNet", function() {
            // arrange
            var dashboard,
                server = new Farmas.TestServer(function(requestOptions) {
                    expect(requestOptions.url).toEqual("server/feanor/RawXmlMessage.aspx");
                    expect(requestOptions.dataType).toEqual("xml");
                    expect(requestOptions.type).toEqual("POST");
                    expect(requestOptions.data).toEqual("action=GetProjectStatus&message=%3cserverMessage+xmlns%3axsi%3d%22http%3a%2f%2fwww.w3.org%2f2001%2fXMLSchema-instance%22+xmlns%3axsd%3d%22http%3a%2f%2fwww.w3.org%2f2001%2fXMLSchema%22+timestamp%3d%22TESTDATE%22+identifier%3d%22ccnet-dashboard%22+server%3d%22TESTHOST%22+source%3d%22CCNET-DASHBOARD%22+%2f%3e");

                    return $.Deferred().resolve({ projects: [] });
                });

            // act / assert
            dashboard = new Farmas.Dashboard({ server: server });
        });

        it("should load available projects from server", function() {
            // arrange
            var server = new Farmas.TestServer(function() {
                var deferred = $.Deferred();
                deferred.resolve({
                    projects: [{
                        name: "My First Project",
                        lastBuildDate: new Date("2012-12-30").toISOString(),
                        activity: { type: "Sleeping" }
                    }]
                });
                return deferred.promise();
            });

            // act
            new Farmas.Dashboard({ server: server });

            // assert
            expect($(".ccnet-project-name").length).toEqual(1);
            expect($(".ccnet-project-name").text()).toEqual("My First Project");
            expect($(".ccnet-project-lastBuildDate").text()).toEqual("Sat Dec 29 2012 16:00:00");
        });

        it("should set stopped class for projects that are stopped", function() {
            // arrange
            var server = new Farmas.TestServer(function() {
                var deferred = $.Deferred();
                deferred.resolve({
                    projects: [{ name: "My First Project", status: "Stopped" }]
                });
                return deferred.promise();
            });

            // act
            new Farmas.Dashboard({ server: server });

            // assert
            expect($(".ccnet-project")).toHaveClass("ccnet-project-stopped");
        });

        it("should set building class for projects that are building", function() {
            // arrange
            var server = new Farmas.TestServer(function() {
                var deferred = $.Deferred();
                deferred.resolve({
                    projects: [{ name: "My First Project", activity: { type: "Building" } }]
                });
                return deferred.promise();
            });

            // act
            new Farmas.Dashboard({ server: server });

            // assert
            expect($(".ccnet-project")).toHaveClass("ccnet-project-building");
        });

        it("should set success class for projects that are not building and they built successfully", function() {
            // arrange
            var server = new Farmas.TestServer(function() {
                var deferred = $.Deferred();
                deferred.resolve({
                    projects: [{ name: "My First Project", buildStatus: "Success", activity: { type: "Sleeping" } }]
                });
                return deferred.promise();
            });

            // act
            new Farmas.Dashboard({ server: server });

            // assert
            expect($(".ccnet-project")).toHaveClass("ccnet-project-success");
        });

        it("should set Fail class for projects that are not building and did not built successfully", function() {
            // arrange
            var server = new Farmas.TestServer(function() {
                var deferred = $.Deferred();
                deferred.resolve({
                    projects: [{ name: "My First Project", buildStatus: "Failure", activity: { type: "Sleeping" } }]
                });
                return deferred.promise();
            });

            // act
            new Farmas.Dashboard({ server: server });

            // assert
            expect($(".ccnet-project")).toHaveClass("ccnet-project-fail");
        });
    });

    describe("udpate", function() {
        it("should update last built date of projects", function() {
            // arrange
            var dashboard,
                count = 0,
                server = new Farmas.TestServer(function() {
                    var deferred = $.Deferred();
                    if (count === 0) {
                        deferred.resolve({
                            projects: [
                                { name: "First Project", lastBuildDate: "1/21/2012" },
                                { name: "Second Project", lastBuildDate: "1/21/2012" }
                            ]
                        });
                    }
                    else {
                        deferred.resolve({
                            projects: [
                                { name: "First Project", lastBuildDate: new Date("2012-12-30").toISOString() },
                                { name: "Second Project", lastBuildDate: new Date("2012-12-31").toISOString() }
                            ]
                        });
                    }
                    count++;
                    return deferred.promise();
                });

            dashboard = new Farmas.Dashboard({ server: server });

            // act
            dashboard.update();

            // assert
            expect($(".ccnet-project-lastBuildDate:eq(0)").text()).toEqual("Sat Dec 29 2012 16:00:00");
            expect($(".ccnet-project-lastBuildDate:eq(1)").text()).toEqual("Sun Dec 30 2012 16:00:00");
        });

        it("should update status of projects", function() {
            // arrange
            var dashboard,
                count = 0,
                server = new Farmas.TestServer(function() {
                    var deferred = $.Deferred();
                    if (count === 0) {
                        deferred.resolve({
                            projects: [
                                { name: "First Project", status: "Running", buildStatus: "Success", activity: { type: "Sleeping" } },
                                { name: "Second Project", status: "Running", buildStatus: "Fail", activity: { type: "Sleeping" } },
                                { name: "Third Project", status: "Running", buildStatus: "Success", activity: { type: "Building" } },
                                { name: "Fourth Project", status: "Running", buildStatus: "Success", activity: { type: "Building" } }
                            ]
                        });
                    }
                    else {
                        deferred.resolve({
                            projects: [
                                { name: "First Project", status: "Running", buildStatus: "Fail", activity: { type: "Sleeping" } },
                                { name: "Second Project", status: "Running", buildStatus: "Fail", activity: { type: "Building" } },
                                { name: "Third Project", status: "Running", buildStatus: "Success", activity: { type: "Sleeping" } },
                                { name: "Fourth Project", status: "Stopped", buildStatus: "Success", activity: { type: "Sleeping" } }
                            ]
                        });
                    }
                    count++;
                    return deferred.promise();
                });

            dashboard = new Farmas.Dashboard({ server: server });

            // act
            dashboard.update();

            // assert
            expect($(".ccnet-project:eq(0)")).toHaveClass("ccnet-project-fail");
            expect($(".ccnet-project:eq(1)")).toHaveClass("ccnet-project-building");
            expect($(".ccnet-project:eq(2)")).toHaveClass("ccnet-project-success");
            expect($(".ccnet-project:eq(3)")).toHaveClass("ccnet-project-stopped");
        });
    });
});