(function(global, $, ko, undefined) {
    var dashboardTemplate =
        "<ul class='ccnet-project-list' data-bind='foreach: projects'>" +
            "<li data-bind='fadeWhileUpdate: isUpdating'>" +
                "<div class='ccnet-project' data-bind='css: { " +
                    "\"ccnet-project-success\": status() === 1, " +
                    "\"ccnet-project-building\": status() === 2, " +
                    "\"ccnet-project-fail\": status() === 3, " +
                    "\"ccnet-project-stopped\": status() === 4 " +
               "}'>" +
                    "<div class='ccnet-close'>X</div>" +
                    "<div class='ccnet-project-name' data-bind='text: projectName'></div>" +
                    "<div class='ccnet-project-lastBuildDate' data-bind='text: lastBuildDateString'></div>" +
                "</div>" +
            "</li>" +
        "</ul>";
        
    function Dashboard(options) {
        var dashboard = new DashboardViewModel(),
            projectsRequestOptions = buildProjectsRequest(),
            settings = $.extend({
                selector: "#dashboardContainer",
                enableAnimation: false,
                server: new Server()
            }, options);
        
        function init() {
            var $container = $(settings.selector);

            $container.on("click", ".ccnet-close", function(evt) {
                var projectName = $(evt.target).next().text(),
                    project = findProject(projectName);

                dashboard.projects.remove(project);
            });

            dashboard.isAnimationEnabled = settings.enableAnimation;

            settings.server.makeRequest(projectsRequestOptions).done(function(data) {
                $.each(data.projects, function(index, project) {
                    dashboard.projects.push(new ProjectViewModel(dashboard, project));
                });
            });

            $container.html(dashboardTemplate);
            ko.applyBindings(dashboard, $container[0]);
        }

        function buildProjectsRequest() {
            return {
                url: "server/feanor/RawXmlMessage.aspx",
                dataType: "xml",
                type: "POST",
                data: "action=GetProjectStatus&message=" +
                    "%3cserverMessage+" +
                        "xmlns%3axsi%3d%22http%3a%2f%2fwww.w3.org%2f2001%2fXMLSchema-instance%22+" +
                        "xmlns%3axsd%3d%22http%3a%2f%2fwww.w3.org%2f2001%2fXMLSchema%22+" +
                        "timestamp%3d%22" + global.Farmas.getCurrentDate() + "%22+" +
                        "identifier%3d%22ccnet-dashboard%22+" +
                        "server%3d%22" + global.Farmas.getHostName() + "%22+" +
                        "source%3d%22CCNET-DASHBOARD%22+" +
                   "%2f%3e"
            };
        }

        function findProject(projectName){
            var matches = $.grep(dashboard.projects(), function(project) { return project.projectName === projectName; });

            return (matches.length >= 0) ? matches[0] : null;
        }

        function updateProject(localProject, remoteProject) {
            localProject.isAnimationEnabled = dashboard.isAnimationEnabled
                 && (localProject.activity() !== remoteProject.activity.type
                     || localProject.buildStatus() !== remoteProject.buildStatus);

            localProject.beginUpdate(
                function() {
                    this.buildStatus(remoteProject.buildStatus);
                    this.lastBuildDate(remoteProject.lastBuildDate);
                    this.projectStatus(remoteProject.status);
                    if (remoteProject.activity) {
                        this.activity(remoteProject.activity.type);
                    }
                });
        }

        this.update = function() {
            settings.server.makeRequest(projectsRequestOptions).done(function(data) {
                $.each(data.projects, function(index, remoteProject) {
                    var localProject = findProject(remoteProject.name);

                    if (localProject) {
                        updateProject(localProject, remoteProject);
                    }
                });
            });
        }

        init();
    }

    function DashboardViewModel() {
        this.projects = ko.observableArray();
        this.isAnimationEnabled = false;
    }

    function ProjectViewModel(dashboard, project) {
        var buildingActivity = "Building",
            stoppedProjectStatus = "Stopped",
            successBuildStatus = "Success";

        this.projectName = project.name;
        this.serverName = project.serverName;
        this.description = project.description;
        this.projectStatus = ko.observable(project.status);
        this.buildStatus = ko.observable(project.buildStatus);
        this.lastBuildDate = ko.observable(project.lastBuildDate);
        this.activity = ko.observable((project.activity) ? project.activity.type : "");

        this.lastBuildDateString = ko.computed(function() {
            var dateString = new Date(this.lastBuildDate()).toString()
                gmtIndex = dateString.lastIndexOf("GMT");

            return dateString.slice(0, gmtIndex - 1);
        }, this);

        this.isAnimationEnabled = dashboard.isAnimationEnabled;
        this.isUpdating = ko.observable(false);
        this.endUpdate = null;
        this.beginUpdate = function(endUpdateCallback) {
            this.endUpdate = function(){
                endUpdateCallback.call(this);
                this.isUpdating(false); 
            };

            if (this.isAnimationEnabled) {
                this.isUpdating(true);
            }
            else {
                this.endUpdate();
            }
        };

        this.status = ko.computed(function() {
            /* Will return one of 3 status codes:
                - 1: Success
                - 2: Building
                - 3: Error
                - 4: Stopped
            */

            if (this.projectStatus() === stoppedProjectStatus) {
                return 4;
            }
            else if (this.activity() === buildingActivity) {
                return 2;
            }
            else if (this.buildStatus() === successBuildStatus) {
                return 1;
            }
            else {
                return 3;
            }
        }, this);
    }

    function Server() {
        this.makeRequest = function (requestOptions) {
            return $.ajax(requestOptions).pipe(function(xml) {
                var json = $.xml2json(xml);
                return {
                    projects: $.isArray(json.project) ? json.project : [json.project]
                };
            });
        };
    }

    function TestServer(makeRequestFunc) {

        function random(num) {
            return Math.floor((Math.random() * num) + 1);
        }

        function generateProject(suffix) {
            return {
                name: "Project " + suffix,
                buildStatus: random(2) === 1 ? "Success" : "Fail",
                status: random(3) === 1 ? "Stopped" : "Running",
                lastBuildDate: new Date().toISOString(),
                activity: {
                    type: random(2) === 1 ? "Sleeping" : "Building",
                }
            };
        };

        function testMakeRequest(requestOptions) {
            var deferred = $.Deferred();
            deferred.resolve({
                projects: [
                    generateProject("One"),
                    generateProject("Two"),
                    generateProject("Three"),
                    generateProject("Four"),
                    generateProject("Five"),
                    generateProject("Six"),
                    generateProject("Seven"),
                    generateProject("Eight")
               ]
            });
            return deferred.promise();
        };

        this.makeRequest = makeRequestFunc || testMakeRequest;
    }

    ko.bindingHandlers.fadeWhileUpdate = {
        update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var isUpdating = ko.utils.unwrapObservable(valueAccessor());

            if (isUpdating) {
                $(element).fadeOut("slow", viewModel.endUpdate.bind(viewModel));
            }
            else {
                $(element).fadeIn();
            }
        }
    }

    global.Farmas = {
        Dashboard: Dashboard,
        TestServer: TestServer,
        getCurrentDate: function() {
            return new Date().toISOString();
        },
        getHostName: function() {
            return window.location.hostname;
        }
    }
})(this, jQuery, ko);