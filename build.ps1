
function replaceScript($html, $scriptId, $scriptPath){
    $scriptContent = get-content $scriptPath
    $scriptContent = $scriptContent -replace "$", "`r`n"
    return $html -replace "<script id=`"$scriptId`".*$", "<script>`r`n$scriptContent`r`n</script>"
}

function replaceCss($html, $href, $cssPath){
    $cssContent = get-content $cssPath
    $cssContent = $cssContent -replace "$", "`r`n"
    return $html -replace "<link.*href=`"$href`".*$", "<style type=`"text/css`">`r`n$cssContent`r`n</style>"
}

$html = get-content src\dashboard.html

$html = replaceCss $html dashboard.css src\dashboard.css
$html = replaceScript $html xml2jsonScript lib\jquery.xml2json.js
$html = replaceScript $html dashboardScript src\dashboard.js

set-content dashboard.html $html


